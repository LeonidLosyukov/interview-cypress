const { defineConfig } = require('cypress');

module.exports = defineConfig({
  e2e: {
    baseUrl: 'https://www.google.com/',
    supportFile: 'cypress/support/e2e.{js,jsx,ts,tsx}',
    specPattern: ['cypress/e2e/**/*.{js, ts}'],
    excludeSpecPattern: [
      '**/plugins/**.js',
      '**/support/**',
    ],
    experimentalSessionAndOrigin: true,
  },
});
